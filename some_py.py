import sqlite3 as lite
from faker import Faker
import struct
from typing import Iterable
from dataclasses import dataclass, astuple
import pprint

# try:
#     con = lite.connect('address_book.db')
#
#     cur = con.cursor()
#     cur.execute('SELECT SQLITE_VERSION()')
#
#     data = cur.fetchone()[0]
#
#     print("SQLITE VERSION: {}".format(data))
#
# except lite.Error:
#     print("Error")
#
# finally:
#     if con:
#         con.close()

# con = lite.connect('address_book.db')
# cur = con.cursor()
#
# users = (
#     (1, 'RENAT', 'NIMOV', 'Yamashev str'),
#     (2, 'ILDAR', 'NIMOV', 'Lipovoi ros  chi'),
#     (3, 'Railia', 'Biktimirova', 'Derevnya Universiadyu')
# )
#
# users = (
#     (1, 'USER1', 'Surname1', 'Address1'),
#     (2, 'USER2', 'Surname2', 'Address2'),
# )
#
# # Convert all data in users to the binary representation
# newArr = ()
# for i in users:
#     tuple_to_append = ()
#     for j in i:
#         if isinstance(j, int):
#             tuple_to_append += (bin(j),)
#         elif isinstance(j, str):
#             tuple_to_append += (''.join(map(bin, bytearray(j, encoding='utf-8'))),)
#     newArr += (tuple_to_append,)
#
# cur.execute("DROP TABLE IF EXISTS users")
# cur.execute("CREATE TABLE IF NOT EXISTS users("
#             "user_id INTEGER PRIMARY_KEY,"
#             "first_name VARCHAR(20) NOT NULL,"
#             "last_name VARCHAR(20) NOT NULL,"
#             "address VARCHAR(20) NOT NULL)")
#
# cur.executemany("INSERT INTO users VALUES(?, ?, ?, ?)", newArr)
# con.commit()

@dataclass
class Person:
    name: str
    surname: str
    street: str

PersonStruct = struct.Struct('50s50s30s')

fake = Faker("en_GB")

def generate_people(n: int) -> Iterable[Person]:
    for _ in range(n):
        yield Person(
            fake.first_name(),
            fake.last_name(),
            fake.street_name()
        )

def from_bytes(buffer: bytes) -> Person:
    return Person (
        *(x.decode("utf-8").rstrip("\x00") for x in PersonStruct.unpack(buffer))
    )

# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None


class Solution:
    def reverseList(self, head, head_prev=None):
        """
        :type head: ListNode
        :rtype: ListNode
        """
        quite Slow recursive approach

#         if head is not None:
#             last_not_none_el = self.reverseList(head.next, head)
#             head.next = head_prev
#             return last_not_none_el
#         else:
#             return head_prev

        # Iterative approach
        # reverse_node = None
        # while head is not None:
        #     head_next = head.next
        #     head.next = reverse_node
        #     reverse_node = head
        #     head = head_next
        # return reverse_node

