# Definition for singly-linked list.
class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution:
    def deleteDuplicates(self, head):
        """
        :type head: ListNode
        :rtype: ListNode
        """
        if head is None: # also if not head works
            return []
        start_node = head
        last_el = head
        head = head.next
        while head is not None:
            if head.val != last_el.val:
                last_el.next = head
                last_el = head
            elif head.val == last_el.val and head.next is None:
                last_el.next = None
            head = head.next
        return start_node

if __name__ == "__main__":
    node1 = ListNode(1)
    node2 = ListNode(1)
    node3 = ListNode(2)
    node4 = ListNode(3)
    node5 = ListNode(3)
    # node6 = ListNode(6)
    #


    sol = Solution()
    sol.deleteDuplicates(node1)