# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None

class Solution:
    def middleNode(self, head):
        """
        :type head: ListNode
        :rtype: ListNode
        """
        node1 = head
        node2 = head
        i = 0
        j = 0
        res = [node1.val]
        while node1.next is not None:
            node1 = node1.next
            res.append(node1.val)
            if i % 2 == 0:
                node2 = node2.next
                j += 1
            i += 1
        if node1.next is None:
            while node2.next is not None:
                node2 = node2.next
            return res[j::]

if __name__ == "__main__":
    node1 = ListNode(4)
    # node2 = ListNode(5)
    # node3 = ListNode(6)
    # node4 = ListNode(8.5)
    # node5 = ListNode(7)
    # node6 = ListNode(6)

    # node1.next = node2
    # node2.next = node3
    # node3.next = node4
    # node4.next = node5
    # node5.next = node6

    sol = Solution()
    sol.middleNode(node1)